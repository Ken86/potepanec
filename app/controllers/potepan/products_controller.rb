class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.
      retrieve_related_products(Settings.max_related_products_display_count).
      includes(master: [:default_price, :images])
  end
end
