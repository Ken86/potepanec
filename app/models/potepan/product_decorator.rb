module Potepan::ProductDecorator
  def retrieve_related_products(max_related_products_retrieve_count)
    Spree::Product.
      in_taxons(taxons).
      where.not(id: id).
      distinct.
      limit(max_related_products_retrieve_count)
  end

  Spree::Product.prepend self
end
