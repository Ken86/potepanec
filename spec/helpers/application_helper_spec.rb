require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe '#full_title' do
    subject { helper.full_title(page_title: title) }

    context "when parameter is nil" do
      let(:title) { nil }

      it { is_expected.to eq 'BIGBAG Store' }
    end

    context "when parametor is ''" do
      let(:title) { '' }

      it { is_expected.to eq 'BIGBAG Store' }
    end

    context "when parametor is []" do
      let(:title) { [] }

      it { is_expected.to eq 'BIGBAG Store' }
    end

    context "when parametor exists" do
      let(:title) { 'BAG' }

      it { is_expected.to eq 'BAG - BIGBAG Store' }
    end
  end
end
