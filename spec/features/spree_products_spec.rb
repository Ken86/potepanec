require 'rails_helper'

RSpec.feature "SpreeProducts", type: :feature do
  feature 'visit product detail page' do
    # 【用語説明】
    # メインの商品：params[:id]に紐づく商品
    # 関連商品：メインの商品と同じtaxonに紐づく商品のうち、メインの商品を除く商品

    shared_context 'メインの商品と関連商品4つが単一のtaxonに紐づく場合' do
      given(:product) { create(:product) }
      given(:related_products) do
        [
          create(:product, name: 'Nike Sportswear',   price: 199),
          create(:product, name: 'Dip Dyed Sweater',  price: 249),
          create(:product, name: 'Scarf Ring Corner', price: 179),
          create(:product, name: 'Sun Buddies',       price: 149),
        ]
      end
      given(:taxon) { create(:taxon, name: 'Apache') }

      background do
        Spree::Classification.create(product: product, taxon: taxon)
        related_products.each do |related_product|
          Spree::Classification.create(product: related_product, taxon: taxon)
        end
      end
    end

    shared_context 'メインの商品と関連商品4つが複数のtaxonに紐づく場合' do
      given(:product) { create(:product) }
      given(:related_products) do
        [
          create(:product, name: 'Nike Sportswear',   price: 199),
          create(:product, name: 'Dip Dyed Sweater',  price: 249),
          create(:product, name: 'Scarf Ring Corner', price: 179),
          create(:product, name: 'Sun Buddies',       price: 149),
        ]
      end
      given(:taxon1) { create(:taxon, name: 'Apache') }
      given(:taxon2) { create(:taxon, name: 'Nginx') }

      background do
        Spree::Classification.create(product: product, taxon: taxon1)
        Spree::Classification.create(product: product, taxon: taxon2)
        related_products.each_with_index do |related_product, i|
          if i < 2
            Spree::Classification.create(product: related_product, taxon: taxon1)
          else
            Spree::Classification.create(product: related_product, taxon: taxon2)
          end
        end
      end
    end

    shared_examples '関連商品のパーシャルでのshared_contextで作成した関連商品4つの表示' do
      scenario '関連商品が4つ表示される' do
        within('#related-products') do
          expect(page).to have_content 'Nike Sportswear'
          expect(page).to have_content '199'
          expect(page).to have_content 'Dip Dyed Sweater'
          expect(page).to have_content '249'
          expect(page).to have_content 'Scarf Ring Corner'
          expect(page).to have_content '179'
          expect(page).to have_content 'Sun Buddies'
          expect(page).to have_content '149'
        end
      end
    end

    context "メインの商品がtaxonに紐付かない場合" do
      given(:product) { create(:product) }

      background do
        visit potepan_product_path(product.id)
      end

      scenario '商品ページに遷移すること' do
        expect(page).to have_current_path potepan_product_path(product.id)
      end

      scenario 'メインの商品が表示される' do
        within('.singleProduct') do
          expect(page).to have_content product.name
          expect(page).to have_content product.display_price
          expect(page).to have_content product.description
        end
      end

      scenario "関連商品のパーシャルが表示されない" do
        expect(page).not_to have_selector '#related-products'
      end

      scenario "'一覧ページへ戻る'リンクが表示されない" do
        expect(page).not_to have_content '一覧ページへ戻る'
      end
    end

    context "メインの商品が単一のtaxonに紐付き かつ 関連商品を4つ持つ場合" do
      include_context 'メインの商品と関連商品4つが単一のtaxonに紐づく場合'
      include_examples '関連商品のパーシャルでのshared_contextで作成した関連商品4つの表示'

      background do
        visit potepan_product_path(product.id)
      end

      it_behaves_like '関連商品のパーシャルでのshared_contextで作成した関連商品4つの表示'

      scenario '商品ページに遷移すること' do
        expect(page).to have_current_path potepan_product_path(product.id)
      end

      scenario '関連商品クリック時に商品ページに遷移すること' do
        within('#related-products') do
          click_on('Nike Sportswear')
        end
        within('.singleProduct') do
          expect(page).to have_content 'Nike Sportswear'
        end
      end

      scenario "'一覧ページへ戻る'リンクが表示される" do
        expect(page).to have_content '一覧ページへ戻る'
      end

      scenario "'一覧ページへ戻る'リンククリック時に、一覧ページが表示される" do
        click_link '一覧ページへ戻る'
        within('#productBox') do
          expect(page).to have_content product.name
          expect(page).to have_content product.display_price
        end
      end
    end

    context "メインの商品が単一のtaxonに紐付き かつ 関連商品を5つ持つ場合" do
      include_context 'メインの商品と関連商品4つが単一のtaxonに紐づく場合'
      include_examples '関連商品のパーシャルでのshared_contextで作成した関連商品4つの表示'

      given(:product5) { create(:product, name: 'Adidas Sportswear', price: 111) }

      background do
        Spree::Classification.create(product: product5, taxon: taxon)
        visit potepan_product_path(product.id)
      end

      scenario '商品ページに遷移すること' do
        expect(page).to have_current_path potepan_product_path(product.id)
      end

      it_behaves_like '関連商品のパーシャルでのshared_contextで作成した関連商品4つの表示'

      scenario '関連商品が4つ以上表示されない' do
        within('#related-products') do
          expect(page).not_to have_content 'Adidas Sportswear'
          expect(page).not_to have_content '111'
        end
      end
    end

    context "メインの商品が複数のtaxonに紐付き かつ 異なる種類のtaxonに紐付く関連商品を4つ持つ場合" do
      include_context 'メインの商品と関連商品4つが複数のtaxonに紐づく場合'
      include_examples '関連商品のパーシャルでのshared_contextで作成した関連商品4つの表示'

      background do
        visit potepan_product_path(product.id)
      end

      scenario '商品ページに遷移すること' do
        expect(page).to have_current_path potepan_product_path(product.id)
      end

      it_behaves_like '関連商品のパーシャルでのshared_contextで作成した関連商品4つの表示'
    end

    context "メインの商品が複数のtaxonに紐付き かつ 異なる種類のtaxonに紐付く関連商品を5つ持つ場合" do
      include_context 'メインの商品と関連商品4つが複数のtaxonに紐づく場合'
      include_examples '関連商品のパーシャルでのshared_contextで作成した関連商品4つの表示'

      given(:product5) { create(:product, name: 'Adidas Sportswear', price: 111) }

      background do
        Spree::Classification.create(product: product5, taxon: taxon2)
        visit potepan_product_path(product.id)
      end

      scenario '商品ページに遷移すること' do
        expect(page).to have_current_path potepan_product_path(product.id)
      end

      it_behaves_like '関連商品のパーシャルでのshared_contextで作成した関連商品4つの表示'

      scenario '関連商品のパーシャルにメインの商品が表示されない' do
        within('#related-products') do
          expect(page).not_to have_content product.name
          expect(page).not_to have_content product.display_price
        end
      end

      scenario '関連商品が4つ以上表示されない' do
        within('#related-products') do
          expect(page).not_to have_content 'Adidas Sportswear'
          expect(page).not_to have_content '111'
        end
      end
    end
  end
end
