require 'rails_helper'

RSpec.feature "SpreeCategories", type: :feature do
  shared_context 'when some products associated with taxon whose id is params[:taxon_id]' do
    before do
      Spree::Classification.create(
        product: product_to_display1, taxon: taxon_to_display_product1
      )
      Spree::Classification.create(
        product: product_to_display2, taxon: taxon_to_display_product1
      )
    end

    given(:taxonomy1) { create(:taxonomy, name: 'Categories') }
    given(:taxon_to_display_product1) do
      create(:taxon, name: 'Bags', parent_id: taxonomy1.root.id)
    end
    given(:product_to_display1) { create(:product, name: 'RUBY ON RAILS TOTE', price: 22.99) }
    given(:product_to_display2) { create(:product, name: 'RUBY ON RAILS BAG', price: 15.99) }
  end

  feature 'visit category page' do
    context "when there is product isn't associated with taxon whose id is params[:taxon_id]" do
      include_context "when some products associated with taxon whose id is params[:taxon_id]"

      given(:taxon_not_to_display_product1) do
        create(:taxon, name: 'Apache', parent: taxonomy1.root)
      end
      given(:product_not_to_display1) do
        create(:product, name: 'RUBY ON RAILS MUG', price: 13.9)
      end

      background do
        Spree::Classification.create(
          product: product_not_to_display1, taxon: taxon_not_to_display_product1
        )
        visit potepan_category_path(taxon_to_display_product1.id)
      end

      scenario 'visits category page' do
        expect(page).to have_current_path potepan_category_path(taxon_to_display_product1.id)
      end

      scenario 'shows names and prices of products associated with taxon' do
        expect(page).to have_content product_to_display1.name
        expect(page).to have_content product_to_display2.name
        expect(page).to have_content product_to_display1.price
        expect(page).to have_content product_to_display2.price
      end

      scenario "doesn't show name and price of product insn't associated with taxon" do
        expect(page).not_to have_content product_not_to_display1.name
        expect(page).not_to have_content product_not_to_display1.price
      end
    end

    context "when there are no products associated with taxon whose id is params[:taxon_id]" do
      include_context "when some products associated with taxon whose id is params[:taxon_id]"

      scenario 'visits category page' do
        product_to_display1.destroy!
        product_to_display2.destroy!
        visit potepan_category_path(taxon_to_display_product1.id)
        expect(page).to have_current_path potepan_category_path(taxon_to_display_product1.id)
      end
    end

    context "when there are some taxonomies and taxons associated with product" do
      include_context "when some products associated with taxon whose id is params[:taxon_id]"

      given(:taxonomy2) { create(:taxonomy, name: 'Brands') }
      given(:taxon2) { create(:taxon, name: 'Node', parent_id: taxonomy1.root.id) }
      given(:taxon3) { create(:taxon, name: 'AWS', parent_id: taxonomy2.root.id) }

      background do
        Spree::Classification.create(product: product_to_display1, taxon: taxon2)
        Spree::Classification.create(product: product_to_display1, taxon: taxon3)
        visit potepan_category_path(taxon_to_display_product1.id)
      end

      scenario 'visits category page' do
        expect(page).to have_current_path potepan_category_path(taxon_to_display_product1.id)
      end

      scenario 'show taxonomies in side bar of category' do
        expect(page).to have_content taxonomy1.name
        expect(page).to have_content taxonomy2.name
      end

      scenario 'expand side bar of category and show taxons', js: true do
        click_link(taxonomy1.name)
        sleep 0.5
        click_link(taxonomy2.name)
        sleep 0.5
        expect(find("#taxonomy#{taxonomy1.id}")).to be_visible
        expect(find("#taxonomy#{taxonomy2.id}")).to be_visible
        expect(find("#taxon#{taxon_to_display_product1.id}")).to be_visible
        expect(find("#taxon#{taxon2.id}")).to be_visible
        expect(find("#taxon#{taxon3.id}")).to be_visible
      end

      scenario 'collapse side bar of category after expand it', js: true do
        # Open side bar
        click_on(taxonomy1.name)
        sleep 0.5
        # Close side bar
        click_on(taxonomy1.name)
        sleep 0.5
        within '#product-category' do
          expect(page).not_to have_content taxon_to_display_product1.name
          expect(page).not_to have_content taxon2.name
          expect(page).not_to have_content taxon3.name
        end
      end
    end

    context "when there is a taxon which isn't associated with any product" do
      include_context "when some products associated with taxon whose id is params[:taxon_id]"

      given!(:taxon4) { create(:taxon, name: 'Node', parent_id: taxonomy1.root.id) }

      background do
        visit potepan_category_path(taxon_to_display_product1.id)
      end

      scenario 'visits category page' do
        expect(page).to have_current_path potepan_category_path(taxon_to_display_product1.id)
      end

      scenario "doesn't show taxon witch isn't associated with product in side bar of category" do
        expect(page).not_to have_content taxon4.name
      end
    end

    context "when no taxons assiciated with params[:taxon_id]" do
      given(:not_exist_taxon_id) { Spree::Taxon.last&.id || + 1 }

      scenario 'redirects to 404 Error page' do
        visit potepan_category_path(not_exist_taxon_id)
        expect(page).to have_content "The page you were looking for doesn't exist."
      end
    end
  end

  feature 'transits from category page' do
    include_context "when some products associated with taxon whose id is params[:taxon_id]"

    context "when transit to category page" do
      background do
        visit potepan_category_path(taxon_to_display_product1.id)
        click_link product_to_display1.name
      end

      scenario 'visits category page' do
        expect(page).to have_current_path potepan_product_path(product_to_display1.id)
      end

      scenario 'transits product detail page and shows product detail info' do
        expect(page).to have_content product_to_display1.name
        expect(page).to have_content product_to_display1.description
      end
    end
  end
end
