require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  describe '#retrieve_related_products' do
    # 【用語説明】
    # 関連商品：レシーバの商品と同じtaxonに紐づく商品のうち、レシーバの商品を除く商品
    context 'レシーバの商品が1つのtaxonに紐づき かつ メソッドの引数に2渡し、関連商品が3つ存在する場合' do
      let(:associated_taxon) { create(:taxon) }
      let(:not_associated_taxon) { create(:taxon) }
      let(:receiver_product) { create(:product, taxons: [associated_taxon]) }
      let!(:related_product1) { create(:product, taxons: [associated_taxon]) }
      let!(:related_product2) { create(:product, taxons: [associated_taxon]) }
      let!(:related_product3) { create(:product, taxons: [associated_taxon]) }
      let!(:not_related_product) { create(:product, taxons: [not_associated_taxon]) }

      it '関連商品を2つを返すこと' do
        expect(receiver_product.retrieve_related_products(2)).
          to contain_exactly(related_product1, related_product2)
      end
    end

    context "レシーバの商品が複数のtaxonに紐づき かつ それらのtaxonに同一の商品が紐づく場合" do
      let(:associated_taxon1) { create(:taxon) }
      let(:associated_taxon2) { create(:taxon) }
      let(:receiver_product) { create(:product, taxons: [associated_taxon1, associated_taxon2]) }
      let!(:related_product) { create(:product, taxons: [associated_taxon1, associated_taxon2]) }

      it '関連商品を重複なしで返すこと' do
        expect(receiver_product.retrieve_related_products(2)).to contain_exactly(related_product)
      end
    end
  end
end
